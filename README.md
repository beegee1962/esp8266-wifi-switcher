This small project shows how the loss of a WiFi connection can be detected by using callbacks and how to switch between two different AP's.

More explanation can be found on [ESP8266 WiFi switcher](http://desire.giesecke.tk)
