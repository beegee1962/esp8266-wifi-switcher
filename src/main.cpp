#include "main.h"

// Flag if OTA was initiated
bool otaInitDone = false;

void setup() {
	initLeds();
	Serial.begin(115200);
	scanWiFi();
	connectInit();
}

void loop() {
	if (!otaInitDone) {
		if (connStatus == CON_GOTIP) {
			initOTA();
			otaInitDone = true;
		}
	}

	// Handle OTA requests
	ArduinoOTA.handle();

	// Check WiFi status
	checkWiFiStatus();
}